# Ng-Pravda (Blog)

Créer une application avec un Router.
C'est un tableau .json (db.json) qui fera office de base de données.

Afin de pouvoir exploiter ce tableau et l'importer dans vos composants/services
il faut ajouter les lignes suivantes à votre tscongig.json

```json
    "resolveJsonModule": true,
    "allowSyntheticDefaultImports": true
```

## Page Home / Accueil : "/"
Page d'accueil dont le contenu n'a pas d'importance, mais elle doit exister
et être accessible depuis la route "/"

## Page "Tous les Articles"
- Afficher une archive avec tous les articles.

- Faire en sorte que l'on remarque facilement les articles "isNew"
qui sont les plus tendances en modifiant leur CSS.

- Proposer un extrait du contenu de l'article mais n'en afficher que les
premières lettres pour forcer l'utilisateur à cliquer ;)

- Afficher les articles par ordre de parution (du plus récent au plus ancien)

## Page "un article"
Doit permettre de consulter le détail d'un article:
- son titre
- tout son contenu
- son image
- les commentaires postés s' il y en a (prévenir s'il n'y en a pas)

- Optionnel : proposer au lecteur une liste d'autres articles (tous SAUF celui sur la page actuelle)

## Page Actualité
Afficher une archive avec uniquement les articles "isNew"
