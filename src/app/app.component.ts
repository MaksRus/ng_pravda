import { ThemeService } from './services/theme.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  isDarkTheme: boolean = false;

  constructor(
    private themeService: ThemeService,
  ){
    this.isDarkTheme = this.themeService.isDarkTheme;
  }

  onToggleTheme() {
    this.themeService.toggleTheme();
    this.isDarkTheme = this.themeService.isDarkTheme;
  }
}
