import { Component, OnInit, Output, EventEmitter } from '@angular/core';

interface Link {
  label: string,
  href: string,
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Output() toggleTheme: EventEmitter<void> = new EventEmitter();

  links: Link[] = [
    {label: "Главная", href: ""},
    {label: "Новости", href: "posts/hot"},
    {label: "Статьи", href: "posts"},
  ];

  constructor(
  ) { }

  ngOnInit(): void {
  }

  onClick() {  
    this.toggleTheme.emit();
  }

}
