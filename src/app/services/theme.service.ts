import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  isDarkTheme: boolean = false;

  constructor() { }

  toggleTheme() {
    this.isDarkTheme = !this.isDarkTheme;
  }
}
