import { Injectable } from '@angular/core';
import { Post } from 'src/models/Post';
import { Comment } from 'src/models/Comment';
import { Author, Gender } from 'src/models/Author';
import POSTS from '../../../db.json';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  posts: Post[] = [];

  constructor() {    
    POSTS.forEach(post => {  
      const newPost = new Post(
        post.id,
        post.title,
        post.content,
        post.isNew,
        new Date(post.publishedAt),
        post.image,
        []
      );

      post.comments.forEach(comment => {
        newPost.addComment(new Comment(
          comment.id,
          comment.text,
          new Author(
            comment.author.id,
            comment.author.name,
            comment.author.gender as Gender,
          ),
        ));
      });

      this.posts.push(newPost);
    })    
  }

  getPosts(): Post[] {
    return this.posts;
  }

  getHotPosts(): Post[] {
    return this.posts.filter(post => post.isNew);
  }

  getPostById(id: number): Post | undefined {
    return this.posts.find(post => post.id === id);
  }

}
