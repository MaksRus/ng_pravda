import { PostService } from 'src/app/services/posts.service';
import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/models/Post';

@Component({
  selector: 'app-post-aside',
  templateUrl: './post-aside.component.html',
  styleUrls: ['./post-aside.component.scss']
})
export class PostAsideComponent implements OnInit {
  @Input() excludedPost: Post | undefined;
  posts: Post[] = [];

  constructor(
    private postService: PostService,
  ) {}
  
  ngOnInit(): void {
    this.posts = this.postService.getPosts();
    
    if (this.excludedPost) {
      this.posts = this.posts.filter(p => p !== this.excludedPost);
    }
  }

}
