import { PostService } from 'src/app/services/posts.service';
import { Component, OnInit } from '@angular/core';
import { Post } from 'src/models/Post';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-show',
  templateUrl: './post-show.component.html',
  styleUrls: ['./post-show.component.scss']
})
export class PostShowComponent implements OnInit {
  post: Post | undefined;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute
  ) {}
  
  ngOnInit(): void {    
    this.route.params.subscribe(params => {
      const id = Number(params['id']);
      this.post = this.postService.getPostById(id);
    });
  }

}
