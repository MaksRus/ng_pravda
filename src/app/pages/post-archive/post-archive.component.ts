import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Post } from 'src/models/Post';
import { PostService } from 'src/app/services/posts.service';

@Component({
  selector: 'app-post-archive',
  templateUrl: './post-archive.component.html',
  styleUrls: ['./post-archive.component.scss']
})
export class PostArchiveComponent implements OnInit {
  posts: Post[] = [];
  isHotOnly: boolean= false;

  constructor(
    private postService: PostService,
    private route: ActivatedRoute,
    ){
    }
      
  ngOnInit(): void {
    this.isHotOnly = this.route.snapshot.url.find(segment => segment.path === "hot") != null;
    this.posts = this.isHotOnly ? this.postService.getHotPosts() : this.postService.getPosts();
    
    this.posts.sort((a, b) => b.publishedAt.getTime() - a.publishedAt.getTime());
  }

}
