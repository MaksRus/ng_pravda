import { ThemeService } from './services/theme.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './ui/header/header.component';
import { PostItemComponent } from './components/post-item/post-item.component';
import { CommentsComponent } from './components/comments/comments.component';
import { CommentItemComponent } from './components/comment-item/comment-item.component';
import { PostShowComponent } from './pages/post-show/post-show.component';
import { PostArchiveComponent } from './pages/post-archive/post-archive.component';
import { PostService } from './services/posts.service';
import { PostAsideComponent } from './post-aside/post-aside.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    PostItemComponent,
    CommentsComponent,
    CommentItemComponent,
    PostShowComponent,
    PostArchiveComponent,
    PostAsideComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [PostService, ThemeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
