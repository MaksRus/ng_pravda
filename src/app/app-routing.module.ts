import { PostShowComponent } from './pages/post-show/post-show.component';
import { PostArchiveComponent } from './pages/post-archive/post-archive.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "posts", component: PostArchiveComponent },
  { path: "posts/hot", component: PostArchiveComponent },
  { path: "posts/:id", component: PostShowComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
