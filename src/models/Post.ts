import { Comment } from "./Comment";

export class Post {
    constructor(
        private _id: number,
        private _title: string,
        private _content: string,
        private _isNew: boolean,
        private _publishedAt: Date,
        private _image: string,
        private _comments: Comment[]
    ){}

    get id() {
        return this._id ;
    }
    get title() {
        return this._title ;
    }
    get content() {
        return this._content ;
    }
    get isNew() {
        return this._isNew ;
    }
    get publishedAt() {
        return this._publishedAt ;
    }

    get image() {
        return this._image;
    }
    
    get comments() {
        return this._comments;
    }

    set comments(comments: Comment[]) {
        this._comments = comments;
    }

    addComment(comment: Comment): void {
        this._comments.push(comment);
    }
}