export type Gender = "M" | "F" | "?";

export class Author {
    constructor(
        private _id: number,
        private _name: string,
        private _gender: Gender,
    ){}

    get id(): number {
        return this._id;
    }

    get name(): string {
        return this._name;
    }

    get gender(): Gender {
        return this._gender;
    }
}