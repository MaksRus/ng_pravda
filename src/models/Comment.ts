import { Author } from "./Author";

export class Comment {
    constructor(
        private _id: number,
        private _text: string,
        private _author: Author,
    ){}

    get id(): number {
        return this._id;
    }

    get text(): string {
        return this._text;
    }

    get author(): Author {
        return this._author;
    }

    set author(author: Author) {
        this._author = author;
    }
}